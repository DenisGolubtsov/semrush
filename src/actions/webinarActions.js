import * as types from './actionTypes';

export const createWebinar = webinar => {
    return {type: types.CREATE_WEBINAR, webinar};
};
