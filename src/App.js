import React, {Component} from 'react';
import logo from './assets/logo.svg';
import './App.css';
import Webinars from './components/Webinars/Webinars';
import {Route} from 'react-router-dom';

class App extends Component {
    render() {
        return (
            <div className="App">
                <header className="App-header">
                    <img src={logo} className="App-header__logo"
                         alt="Semrush logo"/>
                </header>
                <main className="App-main">
                    <Route path='/' component={Webinars}/>
                </main>
            </div>
        );
    }
}

export default App;
