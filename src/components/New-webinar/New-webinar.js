import React from 'react';
import PropTypes from 'prop-types';
import './New-webinar.css';
import close from '../../assets/close.svg';
import Dropzone from 'react-dropzone';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as webinarActions from '../../actions/webinarActions';

class NewWebinar extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            files: null,
            webinar: {}
        };

        this.onDrop = this.onDrop.bind(this);
        this.removeImg = this.removeImg.bind(this);
        this.onSave = this.onSave.bind(this);
        this.updateWebinarState = this.updateWebinarState.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    onDrop(files) {
        this.setState({
            files
        });
    }

    removeImg(event) {
        event.stopPropagation();
        if (!this.state.files || this.state.files.left === 0) {
            return;
        }
        this.setState({
            files: null
        });
    }

    updateWebinarState(event) {
        const webinar = this.state.webinar;
        const field = event.target.name;
        webinar[field] = event.target.value;
        return this.setState({webinar});
    }

    onSave(event) {
        event.preventDefault();
        const webinar = this.state.webinar;
        if (!this.state.files || this.state.files.length === 0) {
            webinar.imageRef = null;
            this.createWebinar(webinar);
        } else {
            this.getBase64(this.state.files[0], webinar);
        }
    }

    createWebinar(webinar) {
        if (!webinar.title) {
            webinar.title = ''
        }
        if (!webinar.description) {
            webinar.description = ''
        }
        console.log('New webinar data is:', webinar);
        this.props.actions.createWebinar(webinar);
        this.props.closeWebinarModal();
    }

    getBase64(file, webinar) {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => {
            webinar.imageRef = reader.result;
            this.createWebinar(webinar);
        };
        reader.onerror = error => {
            console.error('Error: ', error);
            webinar.imageRef = null;
            this.createWebinar(webinar);
        };
    }

    closeModal(event) {
        event.preventDefault();
        this.props.closeWebinarModal();
    }

    render() {
        return (
            <form className="New-webinar" onSubmit={this.onSave}>
                <button className="New-webinar__close" tabIndex="4" onClick={this.closeModal}>
                    <img src={close} alt="Close popup"/>
                </button>
                <h1 className="New-webinar__ttl">Add new</h1>
                <Dropzone onDrop={this.onDrop} accept="image/jpeg, image/png" multiple={false} className="New-webinar__upload">
                    {this.state.files && this.state.files[0] ?
                        <div className="New-webinar__upload-cnt">
                            <button className="New-webinar__upload-basket" title="Delete cover" onClick={this.removeImg}></button>
                            <img src={this.state.files[0].preview} className="New-webinar__upload-img j-upload-img" alt="Webinar cover"/>
                        </div>
                        :
                        <div className="New-webinar__upload-info">
                            <div className="New-webinar__upload-ico"></div>
                            <p className="New-webinar__upload-text">select an image file to upload or drag it here</p>
                        </div>
                    }
                </Dropzone>
                <div className="New-webinar__row">
                    <label className="New-webinar__lbl" htmlFor="new-webinar-title">Title</label>
                    <input
                        type="text"
                        name="title"
                        id="new-webinar-title"
                        tabIndex="1"
                        className="b-input"
                        required
                        onChange={this.updateWebinarState}
                        placeholder="Enter title"/>
                </div>
                <div className="New-webinar__row New-webinar__row_mb_32">
                    <label className="New-webinar__lbl" htmlFor="new-webinar-descr">Description</label>
                    <textarea
                        id="new-webinar-descr"
                        name="description"
                        tabIndex="2"
                        onChange={this.updateWebinarState}
                        className="b-input b-input_height_80"
                        required
                        placeholder="Enter description">
                        </textarea>
                </div>
                <div className="New-webinar__submit">
                    <input
                        type="submit"
                        tabIndex="3"
                        className="b-btn b-btn_width_auto b-btn_theme_webinar-submit"
                        value="Save"
                    />
                </div>
            </form>
        )
    }
}

NewWebinar.propTypes = {
    actions: PropTypes.object.isRequired,
    closeWebinarModal: PropTypes.func.isRequired,
};

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(webinarActions, dispatch)
    };
}

export default connect(null, mapDispatchToProps)(NewWebinar);
