import React from 'react';
import PropTypes from 'prop-types';
import './Webinar.css';

const Webinar = ({webinar}) => {
    return (
        <li className="Webinar">
            <a href={webinar.title} className="Webinar__wrapper">
                <div className="Webinar__header">
                    {webinar.imageRef && webinar.imageRef.indexOf('source.unsplash.com') > -1 ?
                        <img src={`${webinar.imageRef}?${webinar.title.split(' ')[0]}`} alt={webinar.title.slice(0, 10) + '...'}/>
                        :
                        webinar.imageRef && webinar.imageRef.length > 0 ?
                            <img src={webinar.imageRef} alt={webinar.title.slice(0, 10) + '...'}/>
                            :
                            null}
                </div>
                <div className="Webinar__cnt">
                    <h2 className="Webinar__ttl">{webinar.title}</h2>
                    <p className="Webinar__descr">{webinar.description}</p>
                    {webinar.category && webinar.category.length > 0 &&
                    <div className="Webinar__category-lbl">
                        <span className="b-label">{webinar.category}</span>
                    </div>
                    }
                </div>
            </a>
        </li>
    )
};

Webinar.propTypes = {
    webinar: PropTypes.object.isRequired
};


export default Webinar;
