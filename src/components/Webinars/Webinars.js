import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Webinar from '../Webinar/Webinar';
import NewWebinar from '../New-webinar/New-webinar';
import Pagination from 'rc-pagination';
import './Webinars.css';
import 'rc-pagination/dist/rc-pagination.css';
import './rc-pagination-reset.css';
import * as webinarActions from '../../actions/webinarActions';
import WebinarsHeader from './Webinars-header/Webinars-header';
import PromoWebinar from '../Promo-webinar/Promo-webinar';

class Webinars extends React.Component {
    constructor(props, context) {
        super(props, context);
        const webinarsStartPage = this.getUrlParameter('page');
        let currentPage = webinarsStartPage && !isNaN(Number(webinarsStartPage)) ? Number(webinarsStartPage) : 1;
        console.log('currentPage', currentPage);
        let webinarsOnPage = this.props.webinars.slice((currentPage - 1) * 8, currentPage * 8);
        if (webinarsOnPage && webinarsOnPage.length === 0) {
            currentPage = 1;
            webinarsOnPage = this.props.webinars.slice((currentPage - 1) * 8, currentPage * 8);
            this.props.history.push(`?page=1`)
        }
        this.state = {
            currentPage,
            webinarsOnPage,
            showModal: false
        };

        this.onChangePage = this.onChangePage.bind(this);
        this.showUpWebinarModal = this.showUpWebinarModal.bind(this);
        this.closeWebinarModal = this.closeWebinarModal.bind(this);
    }

    componentDidMount() {
        window.onpopstate = () => {
            this.updateWebinarsOnPage();
        };
    }

    componentWillReceiveProps (nextProps) {
        if (!nextProps.webinars) {
            return;
        }
        if (this.props.webinars.length !== nextProps.webinars.length) {
            this.updateWebinarsOnPage(nextProps.webinars);
        }
    }

    componentWillUnmount() {
        window.onpopstate = () => {}
    }

    onChangePage(page) {
        this.setState({
            currentPage: page,
            webinarsOnPage: this.props.webinars.slice((page - 1) * 8, page * 8)
        });
        this.props.history.push(`?page=${page}`)
    };

    showUpWebinarModal() {
        const scrollPosition = document.body.getBoundingClientRect().top;
        document.body.style.position = 'fixed';
        if (scrollPosition) {
            document.body.style.top = scrollPosition + 'px';
        }
        this.setState({
            showModal: true
        })
    };

    closeWebinarModal() {
        document.body.style.position = '';
        document.body.style.top = '';
        this.setState({
            showModal: false
        })
    };

    updateWebinarsOnPage(webinars = this.props.webinars) {
        const webinarsStartPage = this.getUrlParameter('page');
        const currentPage = webinarsStartPage && !isNaN(Number(webinarsStartPage)) ? Number(webinarsStartPage) : 1;
        const webinarsOnPage = webinars.slice((currentPage - 1) * 8, currentPage * 8);
        this.setState({
            webinarsOnPage,
            currentPage
        });
    }

    getUrlParameter(name) {
        name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
        const regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
        const results = regex.exec(this.props.location.search);
        return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
    }

    render() {
        return (
            <div className="Webinars">
                <WebinarsHeader openModal={this.showUpWebinarModal}/>
                {<ul className="Webinars__list">
                    {this.state.webinarsOnPage.map((webinar, id) => {
                        return webinar.promo ? <PromoWebinar key={id} webinar={webinar}/> : <Webinar key={id} webinar={webinar}/>
                    })}
                </ul>}

                <div className="Webinars__pagination">
                    <Pagination showPrevNextJumpers={false} onChange={this.onChangePage} current={this.state.currentPage}
                                defaultPageSize={8}
                                total={this.props.webinars.length}/>
                </div>
                {this.state.showModal ?
                    <div className="Webinars__modal">
                        <div className="Webinars__modal-cnt">
                            <NewWebinar
                                closeWebinarModal={this.closeWebinarModal}
                            />
                        </div>
                    </div>
                    :
                    null
                }
            </div>
        )
    }
}

Webinars.propTypes = {
    webinars: PropTypes.array.isRequired
};



function mapStateToProps(state) {
    return {
        webinars: state.webinars
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: bindActionCreators(webinarActions, dispatch)
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Webinars);
