import React from 'react';
import PropTypes from 'prop-types';
import './Webinars-header.css';

const WebinarsHeader = ({openModal}) => {
    return (
        <div className="Webinars-header">
            <div className="Webinars-header__cnt">
                <h1 className="Webinars-header__ttl">Webinars</h1>
                <p className="Webinars-header__descr">
                    Here you can register and take part in educational webinars conducted by the
                    best digital marketing experts.
                </p>
                <button className="b-btn b-btn_width_auto" onClick={openModal}>Add new</button>
            </div>
        </div>
    )
};

WebinarsHeader.propTypes = {
    openModal: PropTypes.func.isRequired
};

export default WebinarsHeader;
