import React from 'react';

const PromoWebinar = ({webinar}) => {
    return (
        <li className="Webinar Webinar_state_promo">
            <a href={webinar.title} className="Webinar__wrapper Webinar__wrapper_state_promo">
                {webinar.imageRef && webinar.imageRef.indexOf('source.unsplash.com') > -1 ?
                    <img src={`${webinar.imageRef}?${webinar.title.split(' ')[0]}`} alt={webinar.title.slice(0, 10) + '...'}/>
                    :
                    webinar.imageRef && webinar.imageRef.length > 0 ?
                        <img src={webinar.imageRef} alt={webinar.title.slice(0, 10) + '...'}/>
                        :
                        null}
                <div className="Webinar__promo-cnt">
                    <h2 className="Webinar__ttl Webinar__ttl_color_white">{webinar.title}</h2>
                    <p className="Webinar__descr Webinar__descr_color_gray-4">{webinar.description}</p>
                </div>
            </a>
        </li>
    );
};

export default PromoWebinar;
