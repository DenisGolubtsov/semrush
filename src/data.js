export const webinars = [
    {
        title: "HTTPS & SSL Does Not Mean You Have a Secure Website",
        description: "Having an SSL certificate does not mean you have a secure website, and with the new European GDPR regulations",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false
    },
    {
        title: "SEMrush 101",
        description: "In this blog post, I share how to use three easily obtainable PPC reports to help improve your organic search performance.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false

    },
    {
        title: "HTTPS & SSL Does Not Mean You Have a Secure Website",
        description: "Having an SSL certificate does not mean you have a secure website, and with the new European GDPR regulations",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false
    },
    {
        title: "SEMrush Real-Time Site Analysis of VisionDirect",
        description: "Learn more about SEMrush",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "advanced",
        promo: false
    },
    {
        title: "The ABC‘s of Online Reviews: Myths, Facts, and Review Removal",
        description: "I help out on the GMB forum and see lots of people claiming to have fake reviews on a daily basis.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: true
    },
    {
        title: "SEO Surgery India",
        description: "I couldn’t be happier to share with you data and insights from Ghergich & Co.’s latest partnership with SEMrush.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "basic",
        promo: false
    },
    {
        title: "SEARCH MARKETING SCOOP with David Bain #12",
        description: "I help out on the GMB forum and see lots of people claiming to have fake reviews on a daily basis.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false
    },
    {
        title: "Show Me The Links! How to build links on a smaller budget",
        description: "I help out on the GMB forum and see lots of people claiming to have fake reviews on a daily basis.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false
    },
    {
        title: "Influencers and Media Partners: How to amplify the reach of content",
        description: "I help out on the GMB forum and see lots of people claiming to have fake reviews on a daily basis.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false
    },
    {
        title: "The ABC‘s of Online Reviews: Myths, Facts, and Review Removal",
        description: "I help out on the GMB forum and see lots of people claiming to have fake reviews on a daily basis.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false
    },
    {
        title: "Influencers and Media Partners: How to amplify the reach of content",
        description: "I help out on the GMB forum and see lots of people claiming to have fake reviews on a daily basis.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false
    },
    {
        title: "Show Me The Links! How to build links on a smaller budget",
        description: "I help out on the GMB forum and see lots of people claiming to have fake reviews on a daily basis.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: true
    },
    {
        title: "SEMrush 101",
        description: "In this blog post, I share how to use three easily obtainable PPC reports to help improve your organic search performance.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false

    },
    {
        title: "HTTPS & SSL Does Not Mean You Have a Secure Website",
        description: "Having an SSL certificate does not mean you have a secure website, and with the new European GDPR regulations",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false
    },
    {
        title: "SEMrush 101",
        description: "In this blog post, I share how to use three easily obtainable PPC reports to help improve your organic search performance.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false

    },
    {
        title: "SEMrush Real-Time Site Analysis of VisionDirect",
        description: "Learn more about SEMrush",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "advanced",
        promo: false
    },
    {
        title: "SEO Surgery India",
        description: "I couldn’t be happier to share with you data and insights from Ghergich & Co.’s latest partnership with SEMrush.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "basic",
        promo: false
    },
    {
        title: "SEARCH MARKETING SCOOP with David Bain #12",
        description: "I help out on the GMB forum and see lots of people claiming to have fake reviews on a daily basis.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false
    },
    {
        title: "SEMrush Real-Time Site Analysis of VisionDirect",
        description: "Learn more about SEMrush",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "advanced",
        promo: false
    },
    {
        title: "The ABC‘s of Online Reviews: Myths, Facts, and Review Removal",
        description: "I help out on the GMB forum and see lots of people claiming to have fake reviews on a daily basis.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false
    },
    {
        title: "Show Me The Links! How to build links on a smaller budget",
        description: "I help out on the GMB forum and see lots of people claiming to have fake reviews on a daily basis.",
        imageRef: "https://source.unsplash.com/random/400*300",
        category: "",
        promo: false
    }
];

export const webinarsStorageName = 'WEBINARS_STORAGE';
