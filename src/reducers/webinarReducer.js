import * as types from '../actions/actionTypes';
import {webinars, webinarsStorageName} from '../data';

const webinarReducer = (state = getDataFromLocalStorage(), action) => {
    switch (action.type) {
        case types.CREATE_WEBINAR:
            const webinars = [...state, Object.assign({}, action.webinar)];
            storeDataToLocalStorage(webinars);
            return webinars;
        default:
            return state;
    }
};

function getDataFromLocalStorage() {
    if (!window.localStorage[webinarsStorageName]) {
        window.localStorage.setItem(webinarsStorageName, JSON.stringify(webinars));
        return webinars;
    }
    return JSON.parse(window.localStorage.getItem(webinarsStorageName));
}

function storeDataToLocalStorage(data) {
    window.localStorage.setItem(webinarsStorageName, JSON.stringify(data));
}

export default webinarReducer;
