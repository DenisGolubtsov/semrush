import {combineReducers} from 'redux';
import webinars from './webinarReducer';

const rootReducer = combineReducers({
    webinars
});

export default rootReducer;
